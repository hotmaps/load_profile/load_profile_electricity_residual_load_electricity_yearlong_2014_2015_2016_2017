[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687159.svg)](https://doi.org/10.5281/zenodo.4687159)

﻿# country dependent profile for each NUTS0 region for electricity residual_load_electricity (WP 2.7)

In this repository the yearlong_2014_2015_2016_2017 load profile for electricity demand, modelled as part of WP 2.7, is published. However, as not all countries publish data regarding hourly electricity demand/supply, some years/countries are missing

Residual load is calculated using electricity system load and RES generation.
Data regarding electricity demand/system load and RES generation (PV, wind onshore and wind offshore) are publicly available at and taken from “Open Power System Data” platform (OPSD) (Datapackage: time series; package version 2017-07-09). 
Data is not altered with respect to the original source. For use in analyses and models, please mind the S/W time changeover at hour 2090.

## Repository structure

Files:
```

data/hotmaps_task_2.7_load_profile_electricity_residual_load_electricity_yearlong_2014_2015_2016_2017.csv  --country dependent profile for each NUTS0 region for electricity residual_load_electricity
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```
## References
[1] [Open Power System Data](https://doi.org/10.25832/time_series/2018-06-30) Data Package Time series,  2018, Version 2018-06-30. Primary data from various sources, for a complete list see URL.



## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu]( https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors

Matthias Kühnbach, Simon Marwitz, Anna-Lena Klingler <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://isi.fraunhofer.de/)
Fraunhofer ISI, Breslauer Str. 48, 
76139 Karlsruhe

## License
Copyright © 2016-2018: Matthias Kuehnbach, Anna-Lena Klingler, Simon Marwitz

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html



Data regarding RES generation (PV, wind onshore and wind offshore) are publicly available at and taken from “Open Power System Data” platform (OPSD) (Datapackage: time series; package version 2017-07-09)
Data license: 
Data is published under MIT license by Open Power System Data.
 [MIT License](https://opensource.org/licenses/MIT)
For Licensing we refer to https://data.open-power-system-data.org/time_series/2017-07-09/README.md
SPDX-License-Identifier: MIT

License-Text: https://spdx.org/licenses/MIT.html

## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
